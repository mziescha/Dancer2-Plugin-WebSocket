#!/usr/bin/perl
use FindBin;
use lib "$FindBin::Bin/../lib";
use Dancer2;
use Dancer2::Plugin::WebSocket;
get '/' => sub {
		q[
			<!DOCTYPE HTML>
			<html>
			<head>
				<script type="text/javascript" src="/js/mootools-core-1.5.0.js"></script>
				<script type="text/javascript" src="/js/mootools-more-1.5.0.js"></script>
			<script type="text/javascript">
				window.addEvent('domready', function(){
					var ws = null;
					if ("WebSocket" in window)
					{
						var ws = new WebSocket("ws:international-talk.com:3001");
						ws.onmessage = function (evt){
							var data = JSON.decode(JSON.decode(evt.data));

							console.log(data.content);

							var li = document.createElement("li");
							li.appendChild(new Element('p', {'html': data.content+' '}));
							$('chatlist').appendChild(li);

						};
						//ws.onclose = function(){alert("Connection is closed...");};
					}
					else alert("WebSocket NOT supported by your Browser!");

					$('textarea').addEvent('keydown', function(e) {
						if (e.code == 13)
							if (e.control){
								ws.send(JSON.encode('{"content":"' + $('textarea').value + '"}'));
								$('textarea').value = null;
							}
					});

					$('testForm').addEvent('submit', function() {
						document.write("Test Form submitted!");
					});
				});
			</script>
			</head>
			<body>
			<ul id="chatlist">

			</ul>
			<form id="testForm">
			  <textarea id="textarea" placeholder="Try Shift + Enter"></textarea>
			</form>
			</body>
			</html>
    ];
};

websocket;
dance;
